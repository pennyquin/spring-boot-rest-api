/*
 *
 *  Copyright (c) 2018-2020 Givantha Kalansuriya, This source is a part of
 *   Staxrt - sample application source code.
 *   http://staxrt.com
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

package com.pennyquin.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/users")
public class UserController {

  private static final String VALID_EMAIL = "pendmqg@gmail.com";

  @PostMapping("/forgot-password/{email}")
  public ResponseEntity<String> forgotPassword(@PathVariable(value = "email") String email) {

    if (!email.equals(VALID_EMAIL)) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST)
              .body("Your unable to recover your password since you are not registered");
    } else {
      return ResponseEntity.ok().body("We are going to send a link to your email with the new password");
    }
  }


}
