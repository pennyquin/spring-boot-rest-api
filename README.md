
# Forgot Password Test API
Esta API es un servicio REST de prueba para ser pruebas automatizadas
Verfica estaticamente si un correo electronico puede "recuperar contraseņa"

Usuario valido: http://localhost:8080/api/users/forgot-password/pendmqg@gmail.com
Usuario invalido: http://localhost:8080/api/users/forgot-password/{email != pendmqg@gmail.com }

## Despliegue

Este es un proyecto Spring Boot con Maven. Para construir y desplegar automaticamente ejecutar
`mvn spring-boot:run`. El API estara expuesto en el puerto 8080.

Este proyecto debe desplegarse antes de correr las pruebas automatizadas de el
repositorio https://bitbucket.org/pennyquin/spring-boot-rest-api
